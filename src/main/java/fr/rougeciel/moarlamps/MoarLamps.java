package fr.rougeciel.moarlamps;

import fr.rougeciel.moarlamps.common.init.ModBlocks;
import fr.rougeciel.moarlamps.common.init.ModItems;
import fr.rougeciel.moarlamps.common.world.gen.ModOreGen;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(MoarLamps.MOD_ID)
public class MoarLamps {

    public static final Logger LOGGER = LogManager.getLogger();
    public static final String MOD_ID = "moarlamps";

    public MoarLamps() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);

        ModBlocks.BLOCKS.register(FMLJavaModLoadingContext.get().getModEventBus());
        ModItems.ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
        //ModEntityType.ENTITY_TYPES.register(FMLJavaModLoadingContext.get().getModEventBus());

        MinecraftForge.EVENT_BUS.register(this);
    }

    private void setup(final FMLCommonSetupEvent event) {
        ModOreGen.registerOres();
    }

    private void doClientStuff(final FMLClientSetupEvent event) {
        LOGGER.info("Got gale settings {}", event.getMinecraftSupplier().get().gameSettings);
    }

    @SubscribeEvent
    public void onServerStarting(FMLServerStartingEvent event) {
        LOGGER.info("Hello from server starting");
    }

    // Custom ItemGroup TAB
    public static final ItemGroup TAB = new ItemGroup("moarlampstab") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(ModItems.RED_LAMP_ITEM.get());
        }
    };
}
