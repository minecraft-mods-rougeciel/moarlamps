package fr.rougeciel.moarlamps.common.items;

import fr.rougeciel.moarlamps.MoarLamps;
import net.minecraft.item.Item;

public class TungstenNugget extends Item {
    public TungstenNugget() {
        super(new Item.Properties()
                .group(MoarLamps.TAB)
        );
    }
}
