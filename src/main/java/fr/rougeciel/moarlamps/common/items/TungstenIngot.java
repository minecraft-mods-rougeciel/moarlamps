package fr.rougeciel.moarlamps.common.items;

import fr.rougeciel.moarlamps.MoarLamps;
import net.minecraft.item.Item;

public class TungstenIngot extends Item {
    public TungstenIngot() {
        super(new Item.Properties()
                .group(MoarLamps.TAB)
        );
    }
}