package fr.rougeciel.moarlamps.common.init;

import fr.rougeciel.moarlamps.MoarLamps;
import fr.rougeciel.moarlamps.common.items.TungstenIngot;
import fr.rougeciel.moarlamps.common.items.TungstenNugget;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ModItems {

    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, MoarLamps.MOD_ID);

    // Items
    public static final RegistryObject<TungstenIngot> TUNGSTEN_INGOT = ITEMS.register("tungsten_ingot", TungstenIngot::new);
    public static final RegistryObject<TungstenNugget> TUNGSTEN_NUGGET = ITEMS.register("tungsten_nugget", TungstenNugget::new);


    // Block Items
    public static final RegistryObject<Item> WHITE_LAMP_ITEM = ITEMS.register("white_lamp",
            () -> new BlockItem(ModBlocks.WHITE_LAMP.get(), new Item.Properties().group(MoarLamps.TAB)));
    public static final RegistryObject<Item> ORANGE_LAMP_ITEM = ITEMS.register("orange_lamp",
            () -> new BlockItem(ModBlocks.ORANGE_LAMP.get(), new Item.Properties().group(MoarLamps.TAB)));
    public static final RegistryObject<Item> MAGENTA_LAMP_ITEM = ITEMS.register("magenta_lamp",
            () -> new BlockItem(ModBlocks.MAGENTA_LAMP.get(), new Item.Properties().group(MoarLamps.TAB)));
    public static final RegistryObject<Item> LIGHT_BLUE_LAMP_ITEM = ITEMS.register("light_blue_lamp",
            () -> new BlockItem(ModBlocks.LIGHT_BLUE_LAMP.get(), new Item.Properties().group(MoarLamps.TAB)));
    public static final RegistryObject<Item> YELLOW_LAMP_ITEM = ITEMS.register("yellow_lamp",
            () -> new BlockItem(ModBlocks.YELLOW_LAMP.get(), new Item.Properties().group(MoarLamps.TAB)));
    public static final RegistryObject<Item> LIME_LAMP_ITEM = ITEMS.register("lime_lamp",
            () -> new BlockItem(ModBlocks.LIME_LAMP.get(), new Item.Properties().group(MoarLamps.TAB)));
    public static final RegistryObject<Item> PINK_LAMP_ITEM = ITEMS.register("pink_lamp",
            () -> new BlockItem(ModBlocks.PINK_LAMP.get(), new Item.Properties().group(MoarLamps.TAB)));
    public static final RegistryObject<Item> GREY_LAMP_ITEM = ITEMS.register("grey_lamp",
            () -> new BlockItem(ModBlocks.GREY_LAMP.get(), new Item.Properties().group(MoarLamps.TAB)));
    public static final RegistryObject<Item> LIGHT_GREY_ITEM = ITEMS.register("light_grey_lamp",
            () -> new BlockItem(ModBlocks.LIGHT_GREY_LAMP.get(), new Item.Properties().group(MoarLamps.TAB)));
    public static final RegistryObject<Item> CYAN_LAMP_ITEM = ITEMS.register("cyan_lamp",
            () -> new BlockItem(ModBlocks.CYAN_LAMP.get(), new Item.Properties().group(MoarLamps.TAB)));
    public static final RegistryObject<Item> PURPLE_LAMP_ITEM = ITEMS.register("purple_lamp",
            () -> new BlockItem(ModBlocks.PURPLE_LAMP.get(), new Item.Properties().group(MoarLamps.TAB)));
    public static final RegistryObject<Item> BLUE_LAMP_ITEM = ITEMS.register("blue_lamp",
            () -> new BlockItem(ModBlocks.BLUE_LAMP.get(), new Item.Properties().group(MoarLamps.TAB)));
    public static final RegistryObject<Item> BROWN_LAMP_ITEM = ITEMS.register("brown_lamp",
            () -> new BlockItem(ModBlocks.BROWN_LAMP.get(), new Item.Properties().group(MoarLamps.TAB)));
    public static final RegistryObject<Item> GREEN_LAMP_ITEM = ITEMS.register("green_lamp",
            () -> new BlockItem(ModBlocks.GREEN_LAMP.get(), new Item.Properties().group(MoarLamps.TAB)));
    public static final RegistryObject<Item> RED_LAMP_ITEM = ITEMS.register("red_lamp",
            () -> new BlockItem(ModBlocks.RED_LAMP.get(), new Item.Properties().group(MoarLamps.TAB)));
    public static final RegistryObject<Item> BLACK_LAMP_ITEM = ITEMS.register("black_lamp",
            () -> new BlockItem(ModBlocks.BLACK_LAMP.get(), new Item.Properties().group(MoarLamps.TAB)));

    public static final RegistryObject<Item> TUNGSTEN_ORE_ITEM = ITEMS.register("tungsten_ore",
            () -> new BlockItem(ModBlocks.TUNGSTEN_ORE.get(), new Item.Properties().group(MoarLamps.TAB)));


}
