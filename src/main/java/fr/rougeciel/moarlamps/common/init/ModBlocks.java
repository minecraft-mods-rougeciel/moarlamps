package fr.rougeciel.moarlamps.common.init;

import fr.rougeciel.moarlamps.MoarLamps;
import fr.rougeciel.moarlamps.common.blocks.LampBlock;
import fr.rougeciel.moarlamps.common.blocks.TungstenOre;
import net.minecraft.block.Block;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ModBlocks {
    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, MoarLamps.MOD_ID);

    // LAMPS
    public static final RegistryObject<Block> WHITE_LAMP = BLOCKS.register("white_lamp", LampBlock::new);
    public static final RegistryObject<Block> ORANGE_LAMP = BLOCKS.register("orange_lamp", LampBlock::new);
    public static final RegistryObject<Block> MAGENTA_LAMP = BLOCKS.register("magenta_lamp", LampBlock::new);
    public static final RegistryObject<Block> LIGHT_BLUE_LAMP = BLOCKS.register("light_blue_lamp", LampBlock::new);
    public static final RegistryObject<Block> YELLOW_LAMP = BLOCKS.register("yellow_lamp", LampBlock::new);
    public static final RegistryObject<Block> LIME_LAMP = BLOCKS.register("lime_lamp", LampBlock::new);
    public static final RegistryObject<Block> PINK_LAMP = BLOCKS.register("pink_lamp", LampBlock::new);
    public static final RegistryObject<Block> GREY_LAMP = BLOCKS.register("grey_lamp", LampBlock::new);
    public static final RegistryObject<Block> LIGHT_GREY_LAMP = BLOCKS.register("light_grey_lamp", LampBlock::new);
    public static final RegistryObject<Block> CYAN_LAMP = BLOCKS.register("cyan_lamp", LampBlock::new);
    public static final RegistryObject<Block> PURPLE_LAMP = BLOCKS.register("purple_lamp", LampBlock::new);
    public static final RegistryObject<Block> BLUE_LAMP = BLOCKS.register("blue_lamp", LampBlock::new);
    public static final RegistryObject<Block> BROWN_LAMP = BLOCKS.register("brown_lamp", LampBlock::new);
    public static final RegistryObject<Block> GREEN_LAMP = BLOCKS.register("green_lamp", LampBlock::new);
    public static final RegistryObject<Block> RED_LAMP = BLOCKS.register("red_lamp", LampBlock::new);
    public static final RegistryObject<Block> BLACK_LAMP = BLOCKS.register("black_lamp", LampBlock::new);

    // OTHER BLOCKS
    public static final RegistryObject<Block> TUNGSTEN_ORE = BLOCKS.register("tungsten_ore", TungstenOre::new);
}
